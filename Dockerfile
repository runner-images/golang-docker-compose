FROM docker:latest

ENV GOLANG_VERSION 1.12.6

RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates
RUN apk add --no-cache git make musl-dev go

# Configure Go
ENV GOROOT /usr/lib/go
ENV GOPATH /home/go
ENV PATH /home/go/bin:$PATH

RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin

WORKDIR $GOPATH